# Privacy Policy

**Effective Date:** 2024-10-31

**App Name:** *Copy to Clipboard*
**Developer:** *Naetmul*

*Copy to Clipboard* values your privacy. This privacy policy explains how we handle your data. To be clear: this app does not collect, store, use, or share any personal or sensitive user data.

## 1. Data Collection and Usage
Our app does not access, collect, transmit, or store any data from or about users, including but not limited to:
- Personal information (e.g., name, email, phone number)
- Device information (e.g., IP address, device ID, location)
- Health, payment, or contact data

## 2. No Use of Third-Party SDKs
*Copy to Clipboard* does not integrate any third-party code, SDKs, or external services that collect, access, or share user data.

## 3. No Permissions Required
Our app does not request access to any device permissions (such as location, contacts, camera, or storage).

## 4. Data Sharing
Since we do not collect any data, there is no data to share with third parties. Our app does not transfer, sell, or disclose user data to any external entities.

## 5. Changes to This Policy
We reserve the right to update this policy. If there are changes to our privacy practices, we will notify users with an updated policy within the app.

## 6. Contact Us
If you have any questions or concerns about this Privacy Policy, please contact us at *Naetmul+androiddev@gmail.com*.

By using *Copy to Clipboard*, you agree to this privacy policy. Thank you for trusting *Copy to Clipboard*.